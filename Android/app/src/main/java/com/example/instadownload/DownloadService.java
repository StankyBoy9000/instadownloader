package com.example.instadownload;

import android.app.Service;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;
import androidx.annotation.Nullable;

import org.json.JSONArray;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;



public class DownloadService extends Service {

        String image_url = "https://www.instagram.com/p/B4-IK6NHxup/?igshid=1j0d5f1cmozdg&fbclid=IwAR1jss47iT8dJSt6RCeT1T40pcNRQo7NzK2Tv04JSou16FaoEx_NWRVZLuA";

        int duplicateCounter = 0;
        int counter;
        MyServiceThread myServiceThread;

        @Nullable
        @Override
        public IBinder onBind(Intent intent) {
            return null;
        }

        @Override
        public void onCreate() {
            myServiceThread = new MyServiceThread();
            myServiceThread.setRunning(true);
            myServiceThread.start();


            Toast.makeText(this, "The new service was created", Toast.LENGTH_LONG).show();
        }

        @Override
        public int onStartCommand(Intent intent, int flags, int startId) {
            Toast.makeText(this, "Service started", Toast.LENGTH_LONG).show();
            return super.onStartCommand(intent, flags, startId);
        }

        @Override
        public void onDestroy() {
            myServiceThread.setRunning(false);
            Toast.makeText(this,"Service destroyed", Toast.LENGTH_LONG).show();
        }



        private void setCounter(){
            SharedPreferences sp = getSharedPreferences("imageNumber", 0);
            if(sp.getString("id","") == ""){
                this.counter = 0;
            }
            else{
                this.counter = Integer.parseInt(sp.getString("id",""));
            }
            System.out.println(counter + " counter");
            SharedPreferences.Editor e = sp.edit();
            e.putString("id", Integer.toString(++counter));
            e.commit();

        }


    private class MyServiceThread extends Thread {
        ClipboardManager clipboardManager;
        public boolean running;


        public void setRunning (boolean running){
            this.running = running;
            clipboardManager = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
            System.out.println("did something");
            clipboardManager.addPrimaryClipChangedListener(new ClipboardManager.OnPrimaryClipChangedListener(){
                @Override
                public void onPrimaryClipChanged(){
                    System.out.println("Clipboard registred");
                    duplicateCounter++;
                    if( duplicateCounter % 2 ==1){
                        ClipData clipData = clipboardManager.getPrimaryClip();
                        ClipData.Item item = clipData.getItemAt(0);
                        final String copiedLink = item.getText().toString();
                        if(copiedLink.contains("instagram.com")){
                            Thread t = new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    myServiceThread.downloadImage(copiedLink);
                                }
                            });
                            t.start();

                        }
                    }
                }
            });
        }

        private String downloadImage(String param) {
            System.out.println("Download service started");

            JSONArray urlList;
            String currentUrl = param;
            URL url1 = null;
            ArrayList<String> linkArray = new ArrayList<String>();


            try {

                currentUrl = currentUrl.replace("https://www.instagram.com/","");
                currentUrl = URLEncoder.encode(currentUrl, "UTF-8");
                currentUrl = currentUrl.replace("F","qwe123321");
                url1 = new URL("http://192.168.0.105:5000/getImage/" + currentUrl );
                HttpURLConnection connection = (HttpURLConnection) url1.openConnection();
                connection.setRequestMethod("POST");
                connection.setDoOutput(true);
                connection.setConnectTimeout(100000);
                connection.setReadTimeout(100000);
                connection.connect();
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String content = "", line;
                while ((line = rd.readLine()) != null) {
                    content += line + "\n";
                    Thread.sleep(1000);
                }
                try {

                    JSONArray jsonArray = new JSONArray(content);
                    for(int i = 0; i < jsonArray.length(); i++){
                        linkArray.add(jsonArray.get(i).toString());
                    }
                } catch (Throwable t) {
                    Log.e("My App", "Could not parse malformed JSON");
                }
                rd.close();
                connection.disconnect();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            int file_length = 0;
            try {
                for(int i = 0; i < linkArray.size(); i++){
                    URL url = new URL(linkArray.get(i));
                    URLConnection urlConnection = url.openConnection();
                    urlConnection.connect();
                    file_length = urlConnection.getContentLength();
                    File new_folder = new File(getExternalFilesDir(null), "/instagallery");
                    if(!new_folder.exists()){
                        new_folder.mkdir();
                    }
                    setCounter();
                    File input_file = new File(new_folder, "instaImage" + Integer.toString(counter) + ".jpg");
                    counter++;
                    InputStream inputStream = new BufferedInputStream(url.openStream(),8192);
                    byte[] data = new byte[1024];
                    int total = 0;
                    int count = 0;
                    OutputStream outputStream = new FileOutputStream(input_file);
                    while((count = inputStream.read(data))!=-1){
                        total+=count;
                        outputStream.write(data,0,count);
                        int progress = (int) total*100/file_length;
                    }
                    inputStream.close();
                    outputStream.close();
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "Download complete...";
        }
    }





}
