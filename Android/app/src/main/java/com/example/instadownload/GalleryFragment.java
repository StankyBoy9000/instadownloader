package com.example.instadownload;

import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.content.Context;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View.OnTouchListener;
import android.widget.ScrollView;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import java.io.File;
import java.util.ArrayList;

public class GalleryFragment extends Fragment {

    String myData = "";
    File myExternalFile;
    View galleryView;
    ImageView mainImageView;
    ImageView previouslySelectedImage;
    ArrayList<String> imagePaths;
    LinearLayout layout;
    int currentMainImageIndex = 0;
    TextView rightButton;
    TextView leftButton;
    HorizontalScrollView hScrollView;
    ScrollView scrollView;
    int screenHeight;
    int screenWidth;


    public GalleryFragment() {
        // Required empty public constructor
    }


    public static GalleryFragment newInstance(String param1, String param2) {
        GalleryFragment fragment = new GalleryFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        screenHeight = displayMetrics.heightPixels;
        screenWidth = displayMetrics.widthPixels;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
            galleryView = inflater.inflate(R.layout.fragment_gallery, container, false);
            hScrollView = galleryView.findViewById(R.id.galleryhScrollView);

        }
        else if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            galleryView = inflater.inflate(R.layout.fragment_gallery_landscape, container, false);
            scrollView = galleryView.findViewById(R.id.galleryScrollView);
        }

        mainImageView = (ImageView) galleryView.findViewById(R.id.mainImageID);
        layout = (LinearLayout) galleryView.findViewById(R.id.horizontalGalleryLayoutID);
        leftButton = (TextView) galleryView.findViewById(R.id.leftButtonID);
        rightButton = (TextView) galleryView.findViewById(R.id.rightButtonID);
        imagePaths = new ArrayList<String>();
        myExternalFile = new File(getActivity().getExternalFilesDir(null) + "/instagallery", "/instaImage0.jpg");


        getImagesFromStorage();

        rightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getNextImage();
            }
        });

        leftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPreviousImage();
            }
        });

        mainImageView.setOnTouchListener(new OnSwipeTouchListener(getActivity()) {
            @Override
            public void onSwipeTop() {
                if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
                    getPreviousImage();
                }
            }
            @Override
            public void onSwipeBottom(){
                if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
                    getNextImage();
                }
            }
            @Override
            public void onSwipeLeft() {
                if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
                    getNextImage();
                }
            }
            @Override
            public void onSwipeRight() {
                if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
                    getPreviousImage();
                }
            }

        });
        return galleryView;
    }

    private void getNextImage(){
        if(!(currentMainImageIndex >= layout.getChildCount()-1)){
            currentMainImageIndex++;
            selectImage();
            mainImageView.setImageDrawable(Drawable.createFromPath(imagePaths.get(currentMainImageIndex)));
        }
    }

    private void getPreviousImage(){
        if(!(currentMainImageIndex <= 0)){
            currentMainImageIndex--;
            selectImage();
            mainImageView.setImageDrawable(Drawable.createFromPath(imagePaths.get(currentMainImageIndex)));
        }
    }
    private static boolean isExternalStorageReadOnly() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    private static boolean isExternalStorageAvailable() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    private void selectImage(){
        if(previouslySelectedImage == null){
            previouslySelectedImage = (ImageView) layout.getChildAt(currentMainImageIndex);
            previouslySelectedImage.setColorFilter(Color.argb(70, 0, 0, 0));

        }
        else{
            previouslySelectedImage.setColorFilter(Color.argb(0,0,0,0));
            previouslySelectedImage = (ImageView) layout.getChildAt(currentMainImageIndex);
            previouslySelectedImage.setColorFilter(Color.argb(70, 0, 0, 0));
        }
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
            focusOnHView();
        }
        else if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            focusOnView();
        }

    }

    private final void focusOnHView(){
        hScrollView.post(new Runnable() {
            @Override
            public void run() {
                hScrollView.smoothScrollTo(previouslySelectedImage.getRight() - (hScrollView.getWidth()/3)*2, 0);
            }
        });
    }

    private final void focusOnView(){
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.smoothScrollTo(0,previouslySelectedImage.getBottom() - (scrollView.getHeight()/3)*2);
            }
        });
    }


    private void getImagesFromStorage(){
        String path = getActivity().getExternalFilesDir(null) + "/instagallery";
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        File directory = new File(path);
        File[] files = directory.listFiles();
        if(files == null){
            return;
        }
        for (int i = 0; i < files.length; i++)
        {
            final String currentImagePath = path + "/" + files[i].getName();
            imagePaths.add(currentImagePath);
            System.out.println(currentImagePath);
            final ImageView image = new ImageView(getActivity());
            image.setTag(currentImagePath + "-" + i);
            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mainImageView.setImageDrawable(Drawable.createFromPath(v.getTag().toString().split("-",2)[0]));
                    currentMainImageIndex = Integer.parseInt(v.getTag().toString().split("-",2)[1]);
                    selectImage();
                    Drawable highlight = getResources().getDrawable( R.drawable.highlight);
                    image.setBackground(highlight);
                }
            });
            image.setImageDrawable(Drawable.createFromPath(currentImagePath));
            layout.addView(image);
            if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){

                image.getLayoutParams().width = screenWidth/6;
                image.getLayoutParams().height = screenHeight/4;
                System.out.println(image.getLayoutParams().width + " " + image.getLayoutParams().height);
                System.out.println(scrollView.getHeight() + " " + scrollView.getWidth());
            }
            else{
                image.getLayoutParams().width = screenWidth/4;
                image.getLayoutParams().height = screenHeight/6;
                System.out.println(image.getLayoutParams().width + " " + image.getLayoutParams().height);

            }

            image.setScaleType(ImageView.ScaleType.FIT_XY);
        }
        if(imagePaths.size() > 0){
            mainImageView.setImageDrawable(Drawable.createFromPath(imagePaths.get(0)));
        }
        previouslySelectedImage = (ImageView) layout.getChildAt(currentMainImageIndex);
        previouslySelectedImage.setColorFilter(Color.argb(70, 0, 0, 0));
        System.out.println(layout.getChildCount()+ " childCount");
    }

    public class OnSwipeTouchListener implements OnTouchListener {

        private final GestureDetector gestureDetector;

        public OnSwipeTouchListener(Context context) {
            gestureDetector = new GestureDetector(context, new GestureListener());
        }

        public void onSwipeLeft() {
        }

        public void onSwipeRight() {
        }

        public void onSwipeTop() {
        }

        public void onSwipeBottom() {
        }

        public boolean onTouch(View v, MotionEvent event) {
            return gestureDetector.onTouchEvent(event);
        }

        private final class GestureListener extends SimpleOnGestureListener {

            private static final int SWIPE_THRESHOLD = 100;
            private static final int SWIPE_VELOCITY_THRESHOLD = 100;

            @Override
            public boolean onDown(MotionEvent e) {
                return true;
            }

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                boolean result = false;
                try {
                    float diffY = e2.getY() - e1.getY();
                    float diffX = e2.getX() - e1.getX();
                    if (Math.abs(diffX) > Math.abs(diffY)) {
                        if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                            if (diffX > 0) {
                                onSwipeRight();
                            } else {
                                onSwipeLeft();
                            }
                            result = true;
                        }
                    }
                    else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                        if (diffY > 0) {
                            onSwipeBottom();
                        } else {
                            onSwipeTop();
                        }
                        result = true;
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
                return result;
            }
        }
    }
}
