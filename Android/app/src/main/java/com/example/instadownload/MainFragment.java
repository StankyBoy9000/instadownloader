package com.example.instadownload;

import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.Switch;
import android.content.Intent;



public class MainFragment extends Fragment {

    Switch switchButton;
    MainActivity mainActivity;
    View thisView;
    ImageButton openInstagramButton;

    public MainFragment() {

    }


    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainActivity = (MainActivity) getActivity();
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
            thisView = inflater.inflate(R.layout.fragment_main, container, false);
        }
        else if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            thisView = inflater.inflate(R.layout.fragment_main_landscape, container, false);
        }

        switchButton = (Switch) thisView.findViewById(R.id.switchButton);
        openInstagramButton = (ImageButton) thisView.findViewById(R.id.openInstagramButtonID);
        switchButton.setChecked(((MainActivity) getActivity()).getIsServiceActive());

        switchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mainActivity.setIsServiceActive(isChecked);
                if(switchButton.isChecked()){
                    startNewService();
                    ((MainActivity) getActivity()).setIsServiceActive(true);
                    System.out.println(((MainActivity) getActivity()).getIsServiceActive());
                }
                else{
                    stopNewService();
                    ((MainActivity) getActivity()).setIsServiceActive(false);
                    System.out.println(((MainActivity) getActivity()).getIsServiceActive());
                }
            }
        });

        openInstagramButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openInstagramApp();
            }
        });
        return thisView;

    }
    public void startNewService(){
        getActivity().startService(new Intent(getActivity(), DownloadService.class));
    }

    public void stopNewService(){
        getActivity().stopService(new Intent(getActivity(), DownloadService.class));
    }

    private void openInstagramApp(){
        PackageManager manager = getActivity().getPackageManager();
        try {
            Intent intent = manager.getLaunchIntentForPackage("com.instagram.android");
            if (intent == null) {
                throw new PackageManager.NameNotFoundException();
            }
            intent.addCategory(Intent.CATEGORY_LAUNCHER);
            this.startActivity(intent);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }
}
