import flask, json
from flask import Flask, request
from bs4 import BeautifulSoup
import requests
import json
from selenium import webdriver
import time
from urllib.parse import unquote
from selenium.webdriver.chrome.options import Options


app = Flask('__main__', template_folder="", static_folder="", root_path="", static_url_path="")
msgs = []

chrome_options = Options()




#options.binary_location ="./chromedriver"

@app.route('/', methods=['GET'])
def index_page():
    print("hello")
    return ("Hello")

# @app.after_request
# def after_request_func(response):
#     print("after_request is running!")
#     return response

@app.route('/getImage/<string:link>', methods=['GET', 'POST'])
def download_image(link):
    link = str(link).replace("qwe123321","F")
    link = unquote(link)
    link = "https://www.instagram.com/" + link
    print(link)
    print("Pokrenuta skripta za pretragu instagrama.")

    chrome_options.add_argument("--headless")
    driver = webdriver.Chrome(options=chrome_options)
    driver.get(link)
    all_sources = []
    
    while(True):
        
        r = driver.execute_script("return document.documentElement.outerHTML")
        soup = BeautifulSoup(r, 'lxml')
        all_images = soup.find_all('img',{'class':'FFVAD'})
        all_videos = soup.find_all('video',{'class': 'tWeCl'})
        if 'src' in all_images[0]:
            print("got it")
        for i in all_images:
            if i['src'] not in all_sources:
                all_sources.append(i['src'])
        for i in all_videos:
            if i['src'] not in all_sources:
                all_sources.append(i['src'])
        next_buttons = driver.find_elements_by_class_name("_6CZji")
        if(len(next_buttons) < 1):
            break
        next_buttons[0].click()
        time.sleep(1)

    driver.quit()
    
    print("Podaci skinuti. Duzina podataka: " + str(len(all_sources)))
    return json.dumps(all_sources)
app.run("0.0.0.0", 5000)